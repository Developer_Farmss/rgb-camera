package com.tcs.rgbcamera.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.tcs.rgbcamera.R;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.CvType;
import org.opencv.core.Mat;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by harshdeepsingh on 02/02/17.
 */

public class DashboardActivity extends AppCompatActivity {

    private static final String TAG = "DashboardActivity";
    private Button btnSelectImage, btnClickImage;
    private ImageView imgPicture;
    private Mat mat;
    private static final int CAMERA_REQUEST = 1777;
    TextView txtRgbN, txtRgbL1, txtRgbL2, txtRgbC, txtRgbR1, txtRgbR2;
/*
    int j = 1, deltaR, deltaG, deltaB;
    int r_ref = 146, g_ref = 145, b_ref = 147;
*/

    File sdRoot;
    String dir;
    String fileName;

    static {
        if (!OpenCVLoader.initDebug()) {
            Log.i(TAG, "OpenCV loaded successfully!!");
        } else {
            Log.i(TAG, "OpenCV not loaded");
        }
    }

    private BaseLoaderCallback baseLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            if (status == LoaderCallbackInterface.SUCCESS) {
                populate();
            } else {
                super.onManagerConnected(status);
            }
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
        populate();

    }

    private void populate() {
        txtRgbN = (TextView) findViewById(R.id.txtRgbN);
        txtRgbL1 = (TextView) findViewById(R.id.txtRgbL1);
        txtRgbL2 = (TextView) findViewById(R.id.txtRgbL2);
        txtRgbC = (TextView) findViewById(R.id.txtRgbC);
        txtRgbR1 = (TextView) findViewById(R.id.txtRgbR1);
        txtRgbR2 = (TextView) findViewById(R.id.txtRgbR2);
        btnClickImage = (Button) findViewById(R.id.btnClickImage);
        btnSelectImage = (Button) findViewById(R.id.btnSelectImage);
        imgPicture = (ImageView) findViewById(R.id.imgPicture);

        btnClickImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                sdRoot = Environment.getExternalStorageDirectory();
                dir = "/DCIM/Camera App/";
                Date date = new Date();
                DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
                fileName = dateFormat.format(date) + ".jpg";
                final File mkdir = new File(sdRoot, dir);
                if (!mkdir.exists()) {
                    mkdir.mkdirs();
                }
                final File file = new File(sdRoot, dir + fileName);
                /*ImageReader.OnImageAvailableListener listener = new ImageReader.OnImageAvailableListener() {
                    @Override
                    public void onImageAvailable(ImageReader reader) {
                        Image image = null;
                        try {
                            image = reader.acquireLatestImage();
                            ByteBuffer buffer = image.getPlanes()[0].getBuffer();
                            byte[] bytes = new byte[buffer.capacity()];
                            buffer.get(bytes);
                            save(bytes);
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        } finally {
                            if (image != null) {
                                image.close();
                            }
                        }
                    }

                    private void save(byte[] bytes) throws IOException {
                        OutputStream output = null;
                        try {
                            output = new FileOutputStream(file);
                            output.write(bytes);
                        } finally {
                            if (null != output) {
                                output.close();
                            }
                        }
                    }
                };*/
                // file = new File(Environment.getExternalStorageDirectory() + File.separator + "image.jpg");
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));
                Uri contentUri = Uri.fromFile(file);
                Intent mediaIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                mediaIntent.setData(contentUri);
                DashboardActivity.this.sendBroadcast(mediaIntent);
                startActivityForResult(cameraIntent, CAMERA_REQUEST);
            }
        });

        btnSelectImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent pickPhoto = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(pickPhoto, 1);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);

        switch (requestCode) {
            case 1:
                if (resultCode == RESULT_OK) {
                    Uri selectedImage = imageReturnedIntent.getData();
                    try {
                        Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);
                        mat = new Mat(bitmap.getWidth(), bitmap.getHeight(), CvType.CV_8UC4);
                        Utils.bitmapToMat(bitmap, mat);
                        Bitmap bitmap1 = Bitmap.createBitmap(mat.cols(), mat.rows(), Bitmap.Config.ARGB_8888);
                        Utils.matToBitmap(mat, bitmap1);
                        imgPicture.setImageBitmap(bitmap1);
                        //imgPicture.setRotation(90);
                        imageInfo();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                break;
            case CAMERA_REQUEST:
                if (resultCode == RESULT_OK) {
                    File file = new File(sdRoot, dir + fileName);
                    Bitmap photo = decodeSampledBitmapFromFile(file.getAbsolutePath(), 3264, 3264);
                    mat = new Mat(photo.getWidth(), photo.getHeight(), CvType.CV_8UC4);
                    Utils.bitmapToMat(photo, mat);
                    Bitmap picture = Bitmap.createBitmap(mat.cols(), mat.rows(), Bitmap.Config.ARGB_8888);
                    Utils.matToBitmap(mat, picture);
                    imgPicture.setImageBitmap(picture);
                    //imgPicture.setRotation(90);
                    imageInfo();
                }
                break;
        }
    }

    private void imageInfo() {

        double lR1 = 0, lG1 = 0, lB1 = 0;
        double[] lVal1 = {};
        for (int i = 1725; i <= 1775; i++) {
            for (int j = 775; j <= 825; j++) {
                lVal1 = mat.get(i, j);
                lR1 += lVal1[0];
                lG1 += lVal1[1];
                lB1 += lVal1[2];
            }
        }
        int lR1_avg = (int) (lR1 / 2601);
        int lG1_avg = (int) (lG1 / 2601);
        int lB1_avg = (int) (lB1 / 2601);

/*
        deltaR = lR1_avg - r_ref;
        deltaG = lG1_avg - g_ref;
        deltaB = lB1_avg - b_ref;

        int lr1_final = lR1_avg - deltaR;
        int lg1_final = lG1_avg - deltaG;
        int lb1_final = lB1_avg - deltaB;
*/


        txtRgbL1.setText("lR1 : " + lR1_avg + ", lG1 : " + lG1_avg + ", lB1 :" + lB1_avg);
        System.out.print(lR1_avg + " " + lG1_avg + " " + lB1_avg);

        double r = 0, g = 0, b = 0;
        double[] val = {};
        for (int i = 2125; i <= 2175; i++) {
            for (int j = 1175; j <= 1225; j++) {
                val = mat.get(i, j);
                r += val[0];
                g += val[1];
                b += val[2];
            }
        }
        int r_avg = (int) (r / 2601);
        int g_avg = (int) (g / 2601);
        int b_avg = (int) (b / 2601);

/*
        int r_n_final = r_avg - deltaR;
        int g_n_final = g_avg - deltaG;
        int b_n_final = b_avg - deltaB;
*/


        txtRgbN.setText("R_n : " + r_avg + ", G_n : " + g_avg + ", B_n :" + b_avg);
        System.out.print(r_avg + " " + g_avg + " " + b_avg);


        double lR2 = 0, lG2 = 0, lB2 = 0;
        double[] lVal2 = {};
        for (int i = 2125; i <= 2175; i++) {
            for (int j = 775; j <= 825; j++) {
                lVal2 = mat.get(i, j);
                lR2 += lVal2[0];
                lG2 += lVal2[1];
                lB2 += lVal2[2];
            }
        }
        int lR2_avg = (int) (lR2 / 2601);
        int lG2_avg = (int) (lG2 / 2601);
        int lB2_avg = (int) (lB2 / 2601);

/*
        int lr2_final = lR2_avg - deltaR;
        int lg2_final = lG2_avg - deltaG;
        int lb2_final = lB2_avg - deltaB;
*/

        txtRgbL2.setText("lR2 : " + lR2_avg + ", lG2 : " + lG2_avg + ", lB2 :" + lB2_avg);
        System.out.print(lR2_avg + " " + lG2_avg + " " + lB2_avg);

        double cR = 0, cG = 0, cB = 0;
        double[] cVal = {};
        for (int i = 2525; i <= 2575; i++) {
            for (int j = 1175; j <= 1225; j++) {
                cVal = mat.get(i, j);
                cR += cVal[0];
                cG += cVal[1];
                cB += cVal[2];
            }
        }
        int cR_avg = (int) (cR / 2601);
        int cG_avg = (int) (cG / 2601);
        int cB_avg = (int) (cB / 2601);

/*
        int cr_final = cR_avg - deltaR;
        int cg_final = cG_avg - deltaG;
        int cb_final = cB_avg - deltaB;
*/

        txtRgbC.setText("cR : " + cR_avg + ", cG : " + cG_avg + ", cB :" + cB_avg);
        System.out.print(cR_avg + " " + cG_avg + " " + cB_avg);

        double rR1 = 0, rG1 = 0, rB1 = 0;
        double[] rVal1 = {};
        for (int i = 1725; i <= 1775; i++) {
            for (int j = 1575; j <= 1625; j++) {
                rVal1 = mat.get(i, j);
                rR1 += rVal1[0];
                rG1 += rVal1[1];
                rB1 += rVal1[2];
            }
        }
        int rR1_avg = (int) (rR1 / 2601);
        int rG1_avg = (int) (rG1 / 2601);
        int rB1_avg = (int) (rB1 / 2601);

/*
        int rr1_final = rR1_avg - deltaR;
        int rg1_final = rG1_avg - deltaG;
        int rb1_final = rB1_avg - deltaB;
*/

        txtRgbR1.setText("rR1 : " + rR1_avg + ", rG1 : " + rG1_avg + ", rB1 :" + rB1_avg);
        System.out.print(rR1_avg + " " + rG1_avg + " " + rB1_avg);

        double rR2 = 0, rG2 = 0, rB2 = 0;
        double[] rVal2 = {};
        for (int i = 2125; i <= 2175; i++) {
            for (int j = 1575; j <= 1625; j++) {
                rVal2 = mat.get(i, j);
                rR2 += rVal2[0];
                rG2 += rVal2[1];
                rB2 += rVal2[2];
            }
        }
        int rR2_avg = (int) (rR2 / 2601);
        int rG2_avg = (int) (rG2 / 2601);
        int rB2_avg = (int) (rB2 / 2601);

/*
        int rr2_final = rR2_avg - deltaR;
        int rg2_final = rG2_avg - deltaG;
        int rb2_final = rB2_avg - deltaB;
*/

        txtRgbR2.setText("rR2 : " + rR2_avg + ", rG2 : " + rG2_avg + ", rB2 :" + rB2_avg);
        System.out.print(rR2_avg + " " + rG2_avg + " " + rB2_avg);
    }

    private Bitmap decodeSampledBitmapFromFile(String path, int reqWidth, int reqHeight) {
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);

        // Calculate inSampleSize, Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        options.inPreferredConfig = Bitmap.Config.RGB_565;
        int inSampleSize = 1;

        if (height > reqHeight) {
            inSampleSize = Math.round((float) height / (float) reqHeight);
        }
        int expectedWidth = width / inSampleSize;

        if (expectedWidth > reqWidth) {
            //if(Math.round((float)width / (float)reqWidth) > inSampleSize) // If bigger SampSize..
            inSampleSize = Math.round((float) width / (float) reqWidth);
        }

        options.inSampleSize = inSampleSize;

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;

        return BitmapFactory.decodeFile(path, options);
    }


    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
        OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_1_0, this, baseLoaderCallback);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 1:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED
                        && grantResults[1] == PackageManager.PERMISSION_GRANTED && grantResults[2] == PackageManager.PERMISSION_GRANTED) {

                } else {
                    Toast.makeText(this, "Permissions Required!!", Toast.LENGTH_LONG).show();
                    ActivityCompat.finishAffinity(this);
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    public void onBackPressed() {

    }
}
